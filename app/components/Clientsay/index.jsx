"use client";
import Testimonial from "./testimonial";
import Slider from "react-slick";

const Clientsay = () => {
  const items = [
    {
      id: 1,
      name: "Lisa Johnson",
      designation: "Marketing Director at GHI Company",
      des: "We are incredibly impressed with Launchbox’s graphic and video production services. Their team's creativity and attention to detail brought our brand to life through stunning visuals and captivating videos. The final deliverables perfectly captured our brand essence and resonated with our target audience. We are grateful for their contributions to our success",
    },
    {
      id: 2,
      name: "David Thompson",
      designation: "CEO of Insightful Solutions",
      des: "We were in need of insightful research to inform our business strategy, and Launchbox exceeded our expectations. Their team conducted comprehensive market research, providing us with valuable insights that shaped our decision-making process. The level of detail and accuracy presented in their report was impressive. We now have a clearer understanding of our target market and competitors, enabling us to refine our marketing approach and drive business growth",
    },
    {
      id: 3,
      name: "Mark Reynolds",
      designation: "Founder of InnovateNow",
      des: "Working with Launchbox for our app development needs was an absolute delight. Their team's expertise, professionalism, and commitment to excellence were evident from the very beginning. The result was an app that perfectly aligned with our brand image and delivered a seamless user experience. The attention to detail and beautiful design elements have made our app stand out in the market. We are incredibly impressed and would highly recommend their app development services",
    },
    {
      id: 4,
      name: "Mark Davis",
      designation: "Marketing Manager at DEF Enterprises",
      des: "Choosing Launchbox for our app development was one of the best decisions we made. Their team not only had the technical skills to bring our vision to life but also provided valuable insights and recommendations throughout the process. The end result was an app that exceeded our expectations in terms of functionality and design. Their dedication to delivering high-quality work, on-time delivery, and exceptional customer service made them a standout choice. We are thrilled with the outcome and look forward to future collaborations",
    },
  ];

  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    speed: 8000,
    autoplaySpeed: 5000,
    cssEase: "linear",
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
        },
      },
    ],
  };
  return (
    <div className="mx-auto max-w-2xl py-20 px-4s sm:px-6 lg:max-w-7xl lg:px-8">
      <div className="">
        <h3 className="text-navyblue text-center text-4xl lg:text-6xl font-semibold">
          What say clients about us.
        </h3>
        <h4 className="text-lg font-normal text-darkgray text-center mt-4">
          Event madness gathering innoies,& tech enthusiasts in Speced. <br />{" "}
          do more informations.
        </h4>
        <Slider {...settings}>
          {items.map((item) => (
            <div key={item.id}>
              <Testimonial name={item.name} des={item.des} designation={item.designation}/>
            </div>
          ))}
        </Slider>
      </div>
    </div>
  );
};

export default Clientsay;
