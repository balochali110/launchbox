import Image from "next/image";

const Banner = () => {
  return (
    <main className="pt-44 max-[450px]:pt-20" id="home">
      <div className="px-6 lg:px-8">
        <div className="mx-auto max-w-7xl pt-16 sm:pt-20 pb-20">
          <div className="text-center">
            <h1 className="text-4xl font-semibold text-navyblue sm:text-5xl  lg:text-7xl md:4px lh-96">
              Transform Your Ideas into <br /> Powerful Solutions
            </h1>
            <p className="mt-6 text-lg leading-8 text-bluegray">
              We are your secret weapon and your partner in success as we turn
              your aspirations into reality,
              <br /> pixel by pixel, line by line
            </p>
          </div>

          <div className="text-center mt-5">
            <button
              type="button"
              className="text-[18px] w-56 text-black font-medium bg-[#9BE8DA] py-5 px-9 mt-2 leafbutton hover:bg-[#229680] hover:text-white"
            >
              See How
            </button>
          </div>
        </div>
      </div>
    </main>
  );
};

export default Banner;
