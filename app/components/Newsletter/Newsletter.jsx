import React from "react";
// import { Group } from "./Group";
// import "./style.css";

export const SelectDate = () => {
  return (
    <div className="select-date">
      <div className="div">
        <div className="overlap">
          <div className="text-wrapper">AM</div>
        </div>
        <div className="overlap-group-wrapper">
          <div className="overlap-group">
            <div className="confirm-date-time">Confirm Date &amp; Time</div>
          </div>
        </div>
        <div className="group-2">
          <div className="div-wrapper">
            <div className="text-wrapper-2">4</div>
          </div>
          <img className="arrow" alt="Arrow" src="arrow.png" />
          <div className="text-wrapper-3">February 2021</div>
          <img className="img" alt="Arrow" src="image.png" />
          <div className="row">
            <div className="text-wrapper-4">MON</div>
            <div className="text-wrapper-5">1</div>
            <div className="text-wrapper-6">8</div>
            <div className="text-wrapper-7">15</div>
            <div className="text-wrapper-8">22</div>
            <div className="text-wrapper-9">29</div>
          </div>
          <div className="row-2">
            <div className="text-wrapper-10">TUE</div>
            <div className="text-wrapper-11">2</div>
            <div className="text-wrapper-12">9</div>
            <div className="text-wrapper-13">16</div>
            <div className="text-wrapper-14">23</div>
            <div className="text-wrapper-15">30</div>
          </div>
          <div className="row-3">
            <div className="text-wrapper-16">WED</div>
            <div className="text-wrapper-17">3</div>
            <div className="text-wrapper-18">10</div>
            <div className="text-wrapper-19">17</div>
            <div className="text-wrapper-20">24</div>
            <div className="text-wrapper-21">31</div>
          </div>
          <div className="text-wrapper-22">THU</div>
          <div className="text-wrapper-23">11</div>
          <div className="text-wrapper-24">18</div>
          <div className="text-wrapper-25">25</div>
          <div className="text-wrapper-26">1</div>
          <div className="row-4">
            <div className="text-wrapper-27">FRI</div>
            <div className="text-wrapper-28">5</div>
            <div className="text-wrapper-29">12</div>
            <div className="text-wrapper-30">19</div>
            <div className="text-wrapper-31">26</div>
            <div className="text-wrapper-32">2</div>
          </div>
          <div className="row-5">
            <div className="text-wrapper-33">SAT</div>
            <div className="text-wrapper-34">6</div>
            <div className="text-wrapper-35">13</div>
            <div className="text-wrapper-36">20</div>
            <div className="text-wrapper-37">27</div>
            <div className="text-wrapper-38">3</div>
          </div>
          <div className="row-6">
            <div className="text-wrapper-39">SUN</div>
            <div className="text-wrapper-40">7</div>
            <div className="text-wrapper-41">14</div>
            <div className="text-wrapper-42">21</div>
            <div className="text-wrapper-43">28</div>
            <div className="text-wrapper-44">4</div>
          </div>
        </div>
        <div className="overlap-2">
          <div className="text-wrapper-45">00</div>
        </div>
        <div className="overlap-3">
          <div className="text-wrapper-46">00</div>
        </div>
        <div className="text-wrapper-47">-</div>
        <img className="icon-arrows-up-down" alt="Icon arrows up down" src="icon-arrows-up-down.png" />
        <div className="overlap-4">
          <div className="icon-calender-wrapper">
            <img className="icon-calender" alt="Icon calender" src="icon-calender.png" />
          </div>
          <div className="text-wrapper-48">04/5/2023</div>
        </div>
        <div className="overlap-5">
          <div className="icon-clock-outline-wrapper">
            <img className="icon-clock-outline" alt="Icon clock outline" src="icon-clock-outline.png" />
          </div>
          <div className="text-wrapper-49">00:00 AM</div>
        </div>
        <div className="text-wrapper-50">Date</div>
        <div className="text-wrapper-51">Time</div>
        <div className="overlap-wrapper">
          <div
            className="icon-arrow-back-wrapper"
            style={{
              backgroundImage: "url(icon-arrow-back-outline.png)",
            }}
          >
            <img className="icon-arrow-back" alt="Icon arrow back" src="icon-arrow-back-outline-2.png" />
          </div>
        </div>
        {/* <Group
          style={{
            left: "0",
            position: "absolute",
            top: "860px",
          }}
        /> */}
        <div className="vector-wrapper">
          <img className="vector-2" alt="Vector" src="vector-2.png" />
        </div>
      </div>
    </div>
  );
};